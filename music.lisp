;; Chord Progression Generator
;; Copyright (C) 2021  Eric S. Londres

;; This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
;; Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
;; implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
;; License for more details.

;; You should have received a copy of the GNU Affero General Public License along with this program.
;; If not, see <http://www.gnu.org/licenses/>.


(defconstant I '(c e g))		; C major
(defconstant II '(d f a))		; D minor
(defconstant III '(e g b))		; E minor
(defconstant IV '(f a c))		; F major
(defconstant V '(g b d))		; G major
(defconstant VI '(a c e))		; A minor


;;;; Chord Progression
;;; rules
;; start and end at I
;; follow directed cyclic graph to define progression of chords
;; Measures are four quantic notes; phrases should be 4 or 8 measures

(defparameter *progression-graph* (make-hash-table))

(setf (gethash 'I *progression-graph*) '(I II III IV V VI))
(setf (gethash 'II *progression-graph*) '(III V))
(setf (gethash 'III *progression-graph*) '(IV VI))
(setf (gethash 'IV *progression-graph*) '(I II V))
(setf (gethash 'V *progression-graph*) '(I III VI))
(setf (gethash 'VI *progression-graph*) '(II IV))

(defun make-progression (&optional (cur 'I cur-p) (trace '() trace-p))
  "Generate a chord progression by 'bogosearching' the adjacency list for the major key chord"
  (if (and cur-p trace-p (or (eq cur 'I) (eq cur 'IV) (eq cur 'V)) (= 0 (mod (+ 1 (length trace)) 4)))
      ;; we're done if we have a chord progression ending in I, IV, or V with a multiple of 4 chords within
      (append trace (list cur))
      ;; if we're not done, pick a random chord to move to out of the adjacencty list entry for the current chord
      (let ((next (nth (random (length (gethash cur *progression-graph*))) (gethash cur *progression-graph*)))
	    (trace-n (append trace (list cur))))
	(make-progression next trace-n))))
